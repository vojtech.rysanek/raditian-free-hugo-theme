
  (function(w) {
    window.addEventListener("load", function() {
        var observer = window.lozad(".lozad", {
          rootMargin: window.innerHeight / 2 + "px 0px",
          threshold: 0.01
        }); // lazy loads elements with default selector as '.lozad'
        observer.observe();
    });
        
    const scroll = new SmoothScroll('a[href*="#"]');
    $('a.nav-link').on('click', () => {
        const navbar = $('.navbar-collapse');
        if (navbar && navbar.hasClass('show')) {
        $('.navbar-toggler').click();
        }
    })

    // if the class is already set, we're good.
    if (w.document.documentElement.className.indexOf("fonts-loaded") > -1) {
      return;
    }
    var fontA = new w.FontFaceObserver("Inter", {
      weight: 300
    });
    var fontB = new w.FontFaceObserver("Inter", {
      weight: 400
    });
    var fontC = new w.FontFaceObserver("Inter", {
      weight: 500
    });
    var fontD = new w.FontFaceObserver("Inter", {
      weight: 600
    });
    var fontE = new w.FontFaceObserver("Inter", {
      weight: 700
    });
    var fontF = new w.FontFaceObserver("Inter", {
      weight: 900
    });
    w.Promise.all([fontA.load(), fontC.load(), fontF.load()]).then(function() {
      w.document.documentElement.className += " fonts-loaded";
    });
  })(this);

  